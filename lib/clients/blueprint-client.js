"use strict";

const BaseServiceClient = require('base-lib').BaseServiceClient;

class BlueprintClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext, false, {doNotLogPayload: config.doNotLogPayload === false ? false : true});
    this.appId = this.config.appId || 'Blueprintclient'
    this.baseUrl = this.config.service_client_url;
  }

  async get(){
    const me = this;
    try {
      const url = `${me.baseUrl}/v1/blueprint`;
      return await me._get(url, {});
    } catch (err) {
      throw err;
    }
  }


}

module.exports = BlueprintClient;