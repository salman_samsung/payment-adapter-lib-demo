module.exports = {
  LazyPay: {
    AutoDebit: 'AUTO_DEBIT',
    Bearer: 'Bearer ',
    MerchantAccessKey: 'merchantAccessKey',
    TransactionId: 'transactionId',
    Amount: 'amount',
    TransactionRefNo: 'txnRefNo',
    Mobile: 'mobile',
    Email: 'email',
    OTP: 'otp',
    MerchantTxnId: "merchantTxnId",
    MerchantTransactionId: 'merchantTransactionId',
    Success: 'Success',
    GrantTypeRefresh: 'refresh_token',
    PaymentMode: "OTP",
    ErrorCodes: {
      IncorrectOTP: 'LP_INCORRECT_OTP'
    }
  },
  MobiKwik: {
    ExistingUserCheck: 'existingusercheck',
    Debit: 'debit',
    PayLoadType: 'json',
    comment: "Debit",
    RequestCodes: {
      ExistingUserCheck: '500',
      GenerateOTP: '504',
      TokenType: '0',
      GenerateToken: '507',
      RegenerateToken: '507',
      CheckBalance: '501',
      StartTransaction: '503'
    }
  },
  PayU: {
    TransactionS2SFlow: '2',
    PureS2S: 'pureS2S',
    SubmitOTPData: '{"pureS2S":1}',
    VerifyPayment: 'verify_payment',
    CheckOfferStatus: 'check_offer_status',
    Success: 'Success',
    ValidateVPA: 'validateVPA',
    CheckUPITxnStatus: 'check_upi_txn_status',
    InitiateRefund: 'cancel_refund_transaction',
    CheckIsDomestic: 'check_isDomestic',
    UPI: 'UPI'
  },
  BFL: {
    Actions: {
      CheckEligibility: "CheckEligibility",
      InitiateOTP: "InitiateOTP",
      AuthTransaction: "AuthTransaction",
      TransactionStatus: "TransactionStatus",
      CancelledTransaction: "CancelledTransaction"
    },
    PGStatus: {
      L3: "L3",
      ZERO: "0"
    },
    ERRORS: {
      InvalidPlan: "There is no such finance plan available in the system",
      InvalidOrderNo: "INVALID ORDER NUMBER"
    },
    PartnerCode: "SAM"
  },
  Status: {
    Success: "Success",
    Failure: "Failure",
    Pending: "Pending",
    success: "Success",
    pending: "Pending",
    failure: "Failed",
    IN_PROGRESS: "IN_PROGRESS",
    SUCCESS: "SUCCESS",
    FAIL: "FAIL",
    InProgress: "InProgress",
    Cancelled: "Cancelled",
    Initiated: "Initiated",
    Failed: "Failed",
    ONE: 1,
    transactionSuccess: "success",
    transactionPending: "pending",
    RefundInitiated: "RefundInitiated",
    ZERO: 0,
    Valid: "Valid",
    Invalid: "Invalid"
  },
  ResponseCode: {
    One: "1",
    InvalidRequest: 400
  },
  QwikCilver: {
    ResponseCode: {
      InvalidBatchNumber: 10064,
      InvalidCardNumber: 10004,
      InvalidPINNumber: 10119,
      InvalidIdempotentKey: 10312,
      Failed: 1
    },
    Errors: {
      ValidationError: "ValidationError"
    },
    TransactionType: {
      SOFT: 'soft'
    },
    Paths: {
      BatchNumber: "external_attributes.batch_number",
      CardNumber: "payment_info.card_number",
      Amount: "cart.total_price",
      CardAmount: "external_attributes.amount"
    },
    Notes: {
      VldType: 'VldType',
      EGCRDM: 'EGCRDM',
      Amount: 'Amount',
      NetBillValue: 'NetBillValue',
      POSType: 'POSType',
      Magento: 'magento',
      EGCSRDM: 'EGC- SRDM',
      Paymode: 'Paymode',
      EGC: 'eGC'
    }
  },
  ErrorMessage: {
    "DuplicateData": "Duplicate request, with same orderId is already in progress",
    "SchemaError": "One of the request attributes may be missed out or not in the right format!",
    "MissingOrderID": "ORDER ID IS REQUIRED",
    "InvalidUser": "Invalid User",
    "InvalidInput": "The input received was invalid.",
    "checksumMismatch": "checksum mismatch",
    "MessageSent": "Message Sent to xxxxxx999",
    "InvalidTransactionID": "Invalid transaction ID",
    "InvalidCode": "Invalid Code",
    "RefundFailed": "Refund has failed, Kindly contact MobiKwik",
    "InvalidAccessKey": "Invalid merchant access key",
    "INVALIDDEALID":"INVALID DEAL ID",
    "InvalidOffer": "Offer not applicable",
    "InvalidVPA": "Invalid VPA"
  },
  RequestSchema: {
    Paths: {
      Mobile: "user_info.mobile",
      CardNumber: "payment_info.card_number",
      TransactionID: "transaction_context.transaction_id",
      PostalCode: "user_info.address.postal_code",
      TotalPrice: "cart.total_price",
      OTP: "auth_info.otp",
      NameOnCard: "payment_info.name",
      FinancePlanID: "cart.finance_plan_ids[0]",
      OrderID: "external_attributes.orderID",
      DealID: "external_attributes.deal_id",
      LoanAmt: "external_attributes.unique_id.value"
    }
  },
  StatusCode: {
    NotAcceptable: 406,
    InvalidOffer: 'E525',
    InvalidTerminalMissing : 'E601',
    InvalidVPA: 'E121'
  },
  Zero: 0
}