'use strict'

const PaymentLibEnum = require('payment-lib-ind').Enum,
  _ = require('lodash');

let Enums = {
  PaymentMethods: {
    UPI: 'UPI',
    CC: 'CC',
    DC: 'DC',
    NB: 'NB',
    MobiKwik: 'MobiKwik',
    LazyPay: 'LazyPay'
  },
  PaymentGatewayOptions: {
    CreditCard: 'creditcard',
    DebitCard: 'debitcard',
    LazyPay: 'lazypay',
    MobiKwik: 'mobikwik',
    BFL: 'bajajfinserv',
    Paytm: 'paytm',
    Qwikcilver: 'qwikcilver',
    PayU: 'payu',
    Pinelabs: 'pinelabs'
  },
  PayU: {
    PaymentMethods: {
      CC: 'CC',
      DC: 'DC',
      NB: 'NB',
      UPI: 'UPI'
    },
    ResponseStatus: {
      success: 'Success',
      failed: 'Failed'
    }
  },
  LazyPay: {
    ResponseStatus: {
      Success: 'Success',
      Failed: 'Failed'
    }
  },
  MobiKwik: {
    ResponseStatus: {
      SUCCESS: 'Success',
      FAILURE: 'Failed'
    }
  },
  CardPaymentMethods: {
    CreditCard: 'CC',
    DebitCard: 'DC'
  },
  CardType: {
    MasterCard: 'MAST',
    Visa: 'VISA',
    AmericanExpress: 'AMEX'
  }

};

Enums = _.merge(PaymentLibEnum, Enums);

module.exports = Enums
