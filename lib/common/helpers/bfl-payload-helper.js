'use strict'

const BaseHelper = require('base-lib').BaseHelper;
const repoInfo = require('../../../repo-info');
const _ = require('lodash');
const Constants = require('../constants');
const uuid = require('uuid');
const scope = `BFLPayloadHelper#${repoInfo.version}`;

class BFLPayloadHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.bflConfig = config.bfl;
    this.req =
      this.className = `BFLPayloadHelper#${repoInfo.version}`;
  }

  transformToCheckEligibility(requestPayload) {
    let checkEligibility = this.populateCommonParameters(Constants.BFL.Actions.CheckEligibility);
    checkEligibility.OrderNumber = _.get(requestPayload, Constants.RequestSchema.Paths.OrderID);
    checkEligibility.PostalCode = _.get(requestPayload, Constants.RequestSchema.Paths.PostalCode);
    checkEligibility.Amount = _.get(requestPayload, Constants.RequestSchema.Paths.TotalPrice);
    checkEligibility.RequestID = uuid.v4();
    checkEligibility.TnCAcceptance = 'Y';
    checkEligibility.CardNumber = _.get(requestPayload, Constants.RequestSchema.Paths.CardNumber);
    checkEligibility.MobileNumber = _.get(requestPayload, Constants.RequestSchema.Paths.Mobile);
    this.appendAdditionalParams(checkEligibility);
    this.logv2(scope, 'transformToCheckEligibility', 'transform request to Check Eligibility', {});
    return _.omitBy(checkEligibility, _.isUndefined);
  }

  transformToInitiateOTP(requestPayload) {
    let initiateOTP = this.populateCommonParameters(Constants.BFL.Actions.InitiateOTP);
    initiateOTP.CardNumber = _.get(requestPayload, Constants.RequestSchema.Paths.CardNumber);
    initiateOTP.MobNo = _.get(requestPayload, Constants.RequestSchema.Paths.Mobile);
    initiateOTP.OrderNo = _.get(requestPayload, Constants.RequestSchema.Paths.OrderID);
    initiateOTP.RequestID = uuid.v4();
    initiateOTP.CardNumber = _.get(requestPayload, Constants.RequestSchema.Paths.CardNumber);
    this.appendAdditionalParams(initiateOTP);
    this.logv2(scope, 'transformToInitiateOTP', 'transform request to InitiateOTP', {});
    return _.omitBy(initiateOTP, _.isUndefined);
  }

  transformToAuthTransaction(requestPayload) {
    let skuId = _.get(requestPayload, "cart.line_items")[0].sku_id;
    let authTransaction = this.populateCommonParameters(Constants.BFL.Actions.AuthTransaction);
    let financePlan = this.findSchemeByPlanId(requestPayload, _.get(requestPayload, Constants.RequestSchema.Paths.FinancePlanID));
    authTransaction.OrderNumber = _.get(requestPayload, Constants.RequestSchema.Paths.OrderID);
    authTransaction.SchemeID = financePlan.schemeID;
    authTransaction.ManufacturerID = skuId;
    authTransaction.AssetCatID = skuId;
    authTransaction.LoanAmt = _.get(requestPayload, Constants.RequestSchema.Paths.TotalPrice);
    authTransaction.TncAccept = "Y";
    authTransaction.OTPNumber = _.get(requestPayload, Constants.RequestSchema.Paths.OTP);
    authTransaction.NameOnCard = _.get(requestPayload, Constants.RequestSchema.Paths.NameOnCard);
    authTransaction.Tenure = parseInt(financePlan.tenure).toLocaleString(undefined, { minimumIntegerDigits: 2 });
    authTransaction.Pincode = _.get(requestPayload, Constants.RequestSchema.Paths.PostalCode);
    authTransaction.ReqID = uuid.v4();
    authTransaction.SaleType = this.bflConfig.saleType;
    authTransaction.MobNo = '';
    authTransaction.ProductDescription = skuId;
    requestPayload.payment_info.card_number ?    authTransaction.CardNumber = _.get(requestPayload, Constants.RequestSchema.Paths.CardNumber) :   authTransaction.MobNo = _.get(requestPayload, Constants.RequestSchema.Paths.Mobile);
    let trueClientIP = this.rawRequestContext.headers['true-client-ip'];
    let clientIP = this.rawRequestContext.headers['client-ip'];
    let xForwardedFor = this.rawRequestContext.headers['x-forwarded-for'];
    let ip = trueClientIP || clientIP || xForwardedFor;
    if (!ip && this.rawRequestContext.info) {
      ip = this.rawRequestContext.info.remoteAddress || null;
    }
    authTransaction.IPAddress = ip || "127.0.0.1";
    
    this.appendAdditionalParams(authTransaction);
    this.logv2(scope, 'transformToAuthTransaction', 'transform request to AuthTransaction', {});
    return _.omitBy(authTransaction, _.isUndefined);
  }

  findSchemeByPlanId(data, financePlanId) {
    this.logv2(scope, 'findSchemeByPlanId', 'find Scheme by plan id', {});
    let schemes = _.filter(data.payment_option.finance_plans, function (finance_plans) {
      return finance_plans.plan_id === financePlanId;
    });
    if (schemes.length > 0) {
      return {
        schemeID: _.get(schemes[0], 'scheme.scheme_id'),
        tenure: schemes[0].tenure.value
      }
    }
    else {
      throw new Error(Constants.BFL.ERRORS.InvalidPlan);
    }
  }

  transformToGetTransactionStatus(requestPayload) {
    let transactionStatus = this.populateCommonParameters(Constants.BFL.Actions.CheckEligibility);
    transactionStatus.OrderID = _.get(requestPayload, Constants.RequestSchema.Paths.OrderID);
    transactionStatus.CardNumber = "";
    transactionStatus.Amount = _.get(requestPayload, Constants.RequestSchema.Paths.TotalPrice);
    transactionStatus.RequestID = uuid.v4();
    this.appendAdditionalParams(transactionStatus);
    this.logv2(scope, 'transformToGetTransactionStatus', 'transform request to Get TransactionStatus', {});
    return _.omitBy(transactionStatus, _.isUndefined);

  }
  transformToCancelTransaction(requestPayload) {
    let cancelTransaction = this.populateCommonParameters(Constants.BFL.Actions.CancelledTransaction);
    cancelTransaction.OrderNo = _.get(requestPayload, Constants.RequestSchema.Paths.OrderID);
    cancelTransaction.DealID = _.get(requestPayload, Constants.RequestSchema.Paths.DealID);
    cancelTransaction.LoanAmt = _.get(requestPayload, Constants.RequestSchema.Paths.LoanAmt);
    cancelTransaction.RequestID = uuid.v4();
    this.appendAdditionalParams(cancelTransaction);
    this.logv2(scope, 'transformToInitiateRefund', 'transform request to cancel Transaction', {});
    return _.omitBy(cancelTransaction, _.isUndefined);

  }
  populateCommonParameters(action) {
    let commonParameters = {};
    switch (action) {
      case Constants.BFL.Actions.CheckEligibility:
        commonParameters.DealerCode = this.bflConfig.dealerCode;
        commonParameters.Validationkey = this.bflConfig.validationKey;
        break;
      case Constants.BFL.Actions.InitiateOTP:
        commonParameters.DealerID = this.bflConfig.dealerCode;
        commonParameters.ValidationKey = this.bflConfig.validationKey;
        break;
      case Constants.BFL.Actions.AuthTransaction:
        commonParameters.DealerID = this.bflConfig.dealerCode;
        commonParameters.ValidKey = this.bflConfig.validationKey;
        break;
      case Constants.BFL.Actions.CancelledTransaction:
        commonParameters.DealerID = this.bflConfig.dealerCode;
        commonParameters.ValidKey = this.bflConfig.validationKey;
        break;

    }
    this.logv2(scope, 'populateCommonParameters', 'populate Common Parameters', {});
    return commonParameters;
  }

  appendAdditionalParams(requestPayload) {
    this.logv2(scope, 'appendAdditionalParams', 'append Additional Params', {});
    requestPayload.ReqDt1 = "";
    requestPayload.ReqDt2 = "";
    requestPayload.ReqTxt1 = "";
    requestPayload.ReqTxt2 = "";
  }

  formatResponse(response, action) {
    let responsePayload = {
      status: Constants.Status.Pending,
      external_attributes: {}
    }
    switch (action) {
      case Constants.BFL.Actions.AuthTransaction:
        if (response.AuthTxnResponse.Responsecode !== Constants.BFL.PGStatus.ZERO && response.AuthTxnResponse.Responsecode !== Constants.BFL.PGStatus.L3) {
          responsePayload.status = Constants.Status.Failed;
        } else {
          responsePayload.status = Constants.Status.Success;
          responsePayload.external_attributes.deal_id = response.AuthTxnResponse.DEALID;
        }
        responsePayload.message = response.AuthTxnResponse.Errordescription;
        break;
      case Constants.BFL.Actions.TransactionStatus:
        responsePayload.status = response.StatusWSResponse.Responsecode !== Constants.BFL.PGStatus.ZERO ? Constants.Status.Failed : Constants.Status.Success;
        responsePayload.message = response.StatusWSResponse.Errordescription;
        break;
      case response, Constants.BFL.Actions.CancelledTransaction:
        responsePayload.status = response.CancelledTXNResponse.Responsecode !== Constants.BFL.PGStatus.ZERO ? Constants.Status.Failed : Constants.Status.Success;
        responsePayload.message = response.CancelledTXNResponse.Errordescription;
        break;
    }

    this.logv2(scope, 'formatResponse', 'format Response', {});
    return responsePayload;
  }

}
module.exports = BFLPayloadHelper;