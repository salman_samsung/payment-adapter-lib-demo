module.exports = {
  BASE64: "base64",
  BINARY: "binary",
  AES: "AES-",
  CBC: "-CBC",
  MD5: "md5",
  HEX: "hex",
  SHA256: "sha256",
  REFUND: "REFUND",
  PIPE: "|",
  CHECKSUMHASH: "CHECKSUMHASH",
  IV: '@@@@&&&&####$$$$',
  KEY_128: '128',
  KEY_192: '192',
  KEY_256: '256',
  NUM_16: 16,
  NUM_24: 24,
  NUM_32: 32
}