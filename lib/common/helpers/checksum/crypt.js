"use strict";

const crypto = require('crypto'),
  repoInfo = require('../../../../repo-info'),
  ChecksumConstants = require('./checksum-constants'),
  BaseHelper = require('base-lib').BaseHelper;

class Crypt extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.iv = ChecksumConstants.IV;
  }

  encrypt(data, custom_key) {
    let iv = this.iv;
    let key = custom_key;
    let algo = ChecksumConstants.KEY_256;
    switch (key.length) {
      case ChecksumConstants.NUM_16:
        algo = ChecksumConstants.KEY_128;
        break;
      case ChecksumConstants.NUM_24:
        algo = ChecksumConstants.KEY_192;
        break;
      case ChecksumConstants.NUM_32:
        algo = ChecksumConstants.KEY_256;
        break;
    }
    let cipher = crypto.createCipheriv(ChecksumConstants.AES + algo + ChecksumConstants.CBC, key, iv);
    let encrypted = cipher.update(data, ChecksumConstants.BINARY, ChecksumConstants.BASE64);
    encrypted += cipher.final(ChecksumConstants.BASE64);
    return encrypted;
  }

  encryptText(text, key, iv) {
    let algo = ChecksumConstants.KEY_256;
    switch (key.length) {
      case ChecksumConstants.NUM_16:
        algo = ChecksumConstants.KEY_128;
        break;
      case ChecksumConstants.NUM_24:
        algo = ChecksumConstants.KEY_192;
        break;
      case ChecksumConstants.KEY_256:
        algo = ChecksumConstants.KEY_256;
        break;
    }
    let cipher = crypto.createCipheriv(ChecksumConstants.AES + algo + ChecksumConstants.CBC, key, iv);
    let encrypted = cipher.update(text, ChecksumConstants.BINARY, ChecksumConstants.BASE64);
    encrypted += cipher.final(ChecksumConstants.BASE64);
    return encrypted;
  }

  decrypt(data, custom_key) {
    let iv = this.iv;
    let key = custom_key;
    let algo = ChecksumConstants.KEY_256;
    switch (key.length) {
      case ChecksumConstants.NUM_16:
        algo = ChecksumConstants.KEY_128;
        break;
      case ChecksumConstants.NUM_24:
        algo = ChecksumConstants.KEY_192;
        break;
      case ChecksumConstants.NUM_32:
        algo = ChecksumConstants.KEY_256;
        break;
    }
    let decipher = crypto.createDecipheriv(ChecksumConstants.AES + algo + ChecksumConstants.CBC, key, iv);
    let decrypted = decipher.update(data, ChecksumConstants.BASE64, ChecksumConstants.BINARY);
    try {
      decrypted += decipher.final(ChecksumConstants.BINARY);
    } catch (e) {
      this.errorv2(`Crypt#${repoInfo.version}`, 'decrypt', 'Error in decrypting', { e });
    }
    return decrypted;
  }

  genSalt(length) {
    return new Promise(function (resolve) {
      crypto.randomBytes((length * 3.0) / 4.0, function (err, buf) {
        let salt;
        if (!err) {
          salt = buf.toString(ChecksumConstants.BASE64);
        }
        //salt=Math.floor(Math.random()*8999)+1000;
        resolve(salt);
      });
    });
  }

  md5sum(salt, data) {
    return crypto.createHash(ChecksumConstants.MD5).update(salt + data).digest(ChecksumConstants.HEX);
  }

  sha256sum(salt, data) {
    return crypto.createHash(ChecksumConstants.SHA256).update(data + salt).digest(ChecksumConstants.HEX);
  }

  generateSHA256(data) {
    return crypto.createHash(ChecksumConstants.SHA256).update(data).digest(ChecksumConstants.HEX);
  }

  convertHexToByteArray(hexStr) {
    if (!hexStr) {
      return new Uint8Array();
    }
    let arr = [];
    for (let i = 0, len = hexStr.length; i < len; i += 2) {
      arr.push(parseInt(hexStr.substr(i, 2), 16));
    }
    return new Uint8Array(arr);
  }

  createSignature(data, key, type, isUpperCase) {
    let signature = crypto.createHmac(type, new Buffer(key)).update(data).digest('hex');
    if (isUpperCase) {
      return signature.toUpperCase();
    }
    return signature;
  }
}
module.exports = Crypt;
