'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  Constants = require('../constants'),
  repoInfo = require('../../../repo-info'),
  scope = `LazypayPayloadHelper#${repoInfo.version}`,
  _ = require('lodash'),
  Errors = require('../../errors/error');

class LazypayPayloadHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  checkEligibilityHelper(inputPayload) {
    let payLoad = {
      userDetails: {
        mobile: _.get(inputPayload, 'user_info.mobile'),
        email: _.get(inputPayload, 'user_info.email'),
        firstName: _.get(inputPayload, 'user_info.first_name'),
        lastName: _.get(inputPayload, 'user_info.last_name')
      },
      amount: {
        value: _.get(inputPayload, 'cart.total_price'),
        currency: _.get(inputPayload, 'cart.currency')
      },
      source: _.get(inputPayload, 'external_attributes.source'),
      address: {
        street1: _.get(inputPayload, 'user_info.address.line1'),
        street2: _.get(inputPayload, 'user_info.address.line2'),
        city: _.get(inputPayload, 'user_info.address.city'),
        state: _.get(inputPayload, 'user_info.address.state'),
        country: _.get(inputPayload, 'user_info.address.country'),
        zip: _.get(inputPayload, 'user_info.address.postal_code')
      },
      productSkuDetails: [{
        productId: _.map(_.get(inputPayload, 'cart.line_items'), 'sku_id').join(',')
      }],
      customParams: {
        previousTransactionCount: _.get(inputPayload, 'external_attributes.prev_trans_count'),
        onboardingdate: _.get(inputPayload, 'external_attributes.onboardingdate'),
        usersignupdetails: _.get(inputPayload, 'external_attributes.usersignupdetails'),
        IPaddress: _.get(inputPayload, 'external_attributes.ip_address'),
        UserAgent: _.get(inputPayload, 'external_attributes.user_agent'),
        DeviceInfo: _.get(inputPayload, 'external_attributes.device_info')
      }
    };
    this.logv2(scope, 'getPayLoadForCheckEligibility', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  startTransactionHelper(inputPayload) {
    let payLoad = {
      eligibilityResponseId: _.get(inputPayload, 'external_attributes.eligibilityResponseId'),
      userDetails: {
        mobile: _.get(inputPayload, 'user_info.mobile'),
        email: _.get(inputPayload, 'user_info.email'),
        firstName: _.get(inputPayload, 'user_info.first_name'),
        lastName: _.get(inputPayload, 'user_info.last_name')
      },
      amount: {
        value: _.get(inputPayload, 'cart.total_price'),
        currency: _.get(inputPayload, 'cart.currency')
      },
      merchantTxnId: _.get(inputPayload, 'transaction_context.transaction_id'),
      returnUrl: _.get(inputPayload, 'external_attributes.returnUrl'),
      notifyUrl: _.get(inputPayload, 'external_attributes.notifyUrl'),
      source: _.get(inputPayload, 'external_attributes.source'),
      address: {
        street1: _.get(inputPayload, 'user_info.address.line1'),
        street2: _.get(inputPayload, 'user_info.address.line2'),
        city: _.get(inputPayload, 'user_info.address.city'),
        state: _.get(inputPayload, 'user_info.address.state'),
        country: _.get(inputPayload, 'user_info.address.country'),
        zip: _.get(inputPayload, 'user_info.address.postal_code')
      },
      productSkuDetails: [{
        productId: _.map(_.get(inputPayload, 'cart.line_items'), 'sku_id').join(',')
      }],
      customParams: {
        previousTransactionCount: _.get(inputPayload, 'external_attributes.prev_trans_count'),
        onboardingdate: _.get(inputPayload, 'external_attributes.onboardingdate'),
        usersignupdetails: _.get(inputPayload, 'external_attributes.usersignupdetails'),
        IPaddress: _.get(inputPayload, 'external_attributes.ip_address'),
        UserAgent: _.get(inputPayload, 'external_attributes.user_agent'),
        DeviceInfo: _.get(inputPayload, 'external_attributes.device_info')
      }
    };
    this.logv2(scope, 'getPayLoadForStartTransaction', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  submitOTPHelper(inputPayload) {
    let payLoad = {
      paymentMode: Constants.LazyPay.PaymentMode,
      txnRefNo: _.get(inputPayload, 'external_attributes.txnRefNo'),
      otp: _.get(inputPayload, 'auth_info.otp')
    }
    this.logv2(scope, 'getPayLoadForSubmitOTP', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  resendOTPHelper(inputPayload) {
    let payLoad = {
      txnRefNo: _.get(inputPayload, 'external_attributes.txnRefNo')
    };
    this.logv2(scope, 'getPayLoadForResendOTP', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);
  }
  initiateRefundHelper(inputPayload) {
    let payLoad = {
      merchantTxnId: _.get(inputPayload, 'external_attributes.merchantOrderId'),
      amount: _.get(inputPayload, 'external_attributes.unique_id')
    }
    this.logv2(scope, 'initiateRefundHelper', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);

  }
  responseFormatterForPrepareTransaction(response) {
    let payLoad = {};
    payLoad.status = Constants.Status.Success;
    payLoad.message = response.reason;
    payLoad.external_attributes = {
      txnEligibility: _.get(response, 'txnEligibility'),
      eligibilityResponseId: _.get(response, 'eligibilityResponseId')
    }
    this.logv2(scope, 'responseFormatterForPrepareTransaction', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForStartTransaction(response) {
    let payLoad = {};
    payLoad.status = Constants.Status.Success;
    payLoad.message = response.reason;
    payLoad.external_attributes = {
      txnRefNo: _.get(response, 'txnRefNo'),
      lpTxnId: _.get(response, 'lpTxnId'),
      repayConfirmation: _.get(response, 'repayConfirmation'),
      dueDate: _.get(response, 'dueDate')
    }
    this.logv2(scope, 'responseFormatterForStartTransaction', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForSubmitOTP(response) {
    let payLoad = {};
    payLoad.status = Constants.Status.Success;
    payLoad.message = response.reason;
    payLoad.external_attributes = {
      transactionId: _.get(response, 'transactionId'),
      merchantOrderId: _.get(response, 'merchantOrderId'),
      amount: _.get(response, 'amount'),
      responseData: _.get(response, 'responseData'),
      userDetails: _.get(response, 'userDetails'),
      token: _.get(response, 'token')
    }
    this.logv2(scope, 'responseFormatterForSubmitOTP', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForResendOTP(response) {
    let payLoad = {};
    payLoad.status = Constants.Status.Success;
    payLoad.message = response.reason;
    payLoad.external_attributes = {
      status: _.get(response, 'status'),
      otpType: _.get(response, 'otpType'),
      attemptsRemaining: _.get(response, 'attemptsRemaining')
    }
    this.logv2(scope, 'responseFormatterForResendOTP', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForTransactionStatus(response) {
    let payLoad = {};
    if (response.length == 0) {
      throw { transactionStatusError: Errors.ErrorCodes.LazyPay.InvalidTransactionRequest, message: Constants.ErrorMessage.InvalidTransactionID}
    }
    else {
      payLoad.status = response[0].status;
      payLoad.message = response[0].respMessage;
      payLoad.external_attributes = {
        response: response[0]
      }
    }
    this.logv2(scope, 'responseFormatterForTransactionStatus', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }
  responseFormatterForInitiateRefund(response) {
    let payLoad = {};
    payLoad.status = Constants.Status.Success;
    payLoad.message = response.respMessage;
    payLoad.external_attributes = {
      status: _.get(response, 'status'),
      lpTxnId: _.get(response, 'lpTxnId'),
      txnDateTime: _.get(response, 'txnDateTime'),
      amount: _.get(response, 'amount')
    }
    this.logv2(scope, 'responseFormatterForResendOTP', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);

  }

}

module.exports = LazypayPayloadHelper;