'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  Constants = require('../constants'),
  repoInfo = require('../../../repo-info'),
  scope = `MobikwikPayloadHelper#${repoInfo.version}`,
  Errors = require('../../errors/error'),
  Enums = require('../enum'),
  crypto = require('crypto'),
  _ = require('lodash');

class MobikwikPayloadHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.mobikwikConfig = config.mobikwik;
  }
  startTransactionHelper(inputPayload) {
    let me = this;
    const checksumInputText = `'${_.get(inputPayload, 'cart.total_price')}''${(_.get(inputPayload, 'user_info.mobile') || _.get(inputPayload, 'user_info.email'))}''${Constants.MobiKwik.comment}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.StartTransaction}''${_.get(inputPayload, 'transaction_context.transaction_id')}''${(_.get(inputPayload, 'auth_info.otp') || _.get(inputPayload, 'auth_info.token'))}''${Constants.MobiKwik.Debit}'`;
    let payload = {
      merchantname: me.mobikwikConfig.merchantName,
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.StartTransaction,
      checksum: me._createChecksum(checksumInputText),
      otp: _.get(inputPayload, 'auth_info.otp'),
      email: _.get(inputPayload, 'user_info.email'),
      cell: _.get(inputPayload, 'user_info.mobile'),
      amount: _.get(inputPayload, 'cart.total_price'),
      orderid: _.get(inputPayload, 'transaction_context.transaction_id'),
      comment: Constants.MobiKwik.comment,
      txntype: Constants.MobiKwik.Debit
    };
    this.logv2(scope, 'startTransactionHelper', 'transform request to start transaction', {});
    return _.omitBy(payload, _.isUndefined);
  }

  OTPGenerationHelper(inputPayload) {
    let me = this;
    const checksumInputText = `'${_.get(inputPayload, 'cart.total_price')}''${(_.get(inputPayload, 'user_info.mobile') || _.get(inputPayload, 'user_info.email'))}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.GenerateOTP}''${Constants.MobiKwik.RequestCodes.TokenType}'`;
    let payload = {
      email: _.get(inputPayload, 'user_info.email'),
      cell: _.get(inputPayload, 'user_info.mobile'),
      amount: _.get(inputPayload, 'cart.total_price'),
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.GenerateOTP,
      tokentype: Constants.MobiKwik.RequestCodes.TokenType,
      checksum: me._createChecksum(checksumInputText),
      merchantname: me.mobikwikConfig.merchantName

    }
    this.logv2(scope, 'OTPGenerationHelper', 'transform request to Generate Otp', {});
    return _.omitBy(payload, _.isUndefined);
  }

  tokenGerationHelper(inputPayload) {
    let me = this;
    const checksumInputText = `'${inputPayload.cart.total_price}''${(inputPayload.user_info.mobile || inputPayload.user_info.email)}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.GenerateToken}''${inputPayload.auth_info.otp}''${Constants.MobiKwik.RequestCodes.TokenType}'`;
    let payload = {
      email: _.get(inputPayload, 'user_info.email'),
      cell: _.get(inputPayload, 'user_info.mobile'),
      amount: _.get(inputPayload, 'cart.total_price'),
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.GenerateToken,
      tokentype: Constants.MobiKwik.RequestCodes.TokenType,
      checksum: me._createChecksum(checksumInputText),
      otp: _.get(inputPayload, 'auth_info.otp')
    }
    this.logv2(scope, 'tokenGerationHelper', 'transform request to Generate token', {});
    return payload;
  }

  checkBalanceHelper(inputPayload) {
    let me = this;
    const checksumInputText = `'${(inputPayload.user_info.mobile || inputPayload.user_info.email)}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.CheckBalance}''${(inputPayload.auth_info.otp || inputPayload.auth_info.token)}'`;
    let payload = {
      email: _.get(inputPayload, 'user_info.email'),
      cell: _.get(inputPayload, 'user_info.mobile'),
      amount: _.get(inputPayload, 'cart.total_price'),
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.CheckBalance,
      tokentype: Constants.MobiKwik.RequestCodes.TokenType,
      checksum: me._createChecksum(checksumInputText)
    }
    this.logv2(scope, 'checkBalanceHelper', 'transform request to check Balance', {});
    return payload;
  }

  checkExitingUserHelper(inputpayload) {
    let me = this;
    const checksumInputText = `'${Constants.MobiKwik.ExistingUserCheck}''${(inputpayload.user_info.mobile || inputpayload.user_info.email)}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.ExistingUserCheck}'`;
    let payload = {
      email: _.get(inputpayload, 'user_info.email'),
      cell: _.get(inputpayload, 'user_info.mobile'),
      merchantname: me.mobikwikConfig.merchantName,
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.ExistingUserCheck,
      checksum: me._createChecksum(checksumInputText),
      action: Constants.MobiKwik.ExistingUserCheck
    }
    this.logv2(scope, 'checkExitingUserHelper', 'transform request to check Existing User', {});
    return payload;
  }

  regenerateTokenHelper(inputpayload) {
    let me = this;
    const checksumInputText = `'${(inputpayload.cell || inputpayload.email)}''${me.mobikwikConfig.merchantName}''${me.mobikwikConfig.merchantId}''${Constants.MobiKwik.RequestCodes.RegenerateToken}''${inputpayload.token}''${Constants.MobiKwik.RequestCodes.TokenType}'`;
    let payload = {
      email: _.get(inputpayload, 'email'),
      cell: _.get(inputpayload, 'cell'),
      merchantname: me.mobikwikConfig.merchantName,
      mid: me.mobikwikConfig.merchantId,
      msgcode: Constants.MobiKwik.RequestCodes.GenerateToken,
      checksum: me._createChecksum(checksumInputText),
      tokentype: Constants.MobiKwik.RequestCodes.TokenType,
      token: _.get(inputpayload, 'token')
    }
    this.logv2(scope, 'regenerateTokenHelper', 'transform request to regenerate Token', {});
    return payload;
  }

  getTransactionStatusHelper(inputpayload) {
    const checksumInputText = `'${this.mobikwikConfig.merchantId}''${_.get(inputpayload, 'transaction_context.transaction_id')}'`;
    let payload = {
      mid: this.mobikwikConfig.merchantId,
      checksum: this._createChecksum(checksumInputText),
      orderid: _.get(inputpayload, 'transaction_context.transaction_id')
    }
    this.logv2(scope, 'getTransactionStatusHelper', 'transform request to get Transaction Status', {});
    return payload;
  }

  getRefundHelper(inputpayload) {
    const checksumInputText = `'${this.mobikwikConfig.merchantId}''${_.get(inputpayload, 'transaction_context.transaction_id')}''${_.get(inputpayload, 'external_attributes.amount')}'`;
    let payload = {
      mid: this.mobikwikConfig.merchantId,
      checksum: this._createChecksum(checksumInputText),
      txid: _.get(inputpayload, 'transaction_context.transaction_id'),
      amount: _.get(inputpayload, 'external_attributes.amount')
    }
    this.logv2(scope, 'getRefundHelper', 'transform request to get refund', {});
    return payload;

  }
  _createChecksum(text, useAlternateKey) {
    let me = this;
    this.logv2(scope, '_createChecksum', 'MobiKwik create checksum', {});
    return crypto.createHmac('sha256', useAlternateKey ? me.mobikwikConfig.tokenRegenerateSecretKey : me.mobikwikConfig.secretKey).update(text).digest('hex');
  }

  getChecksum(requestParams) {
    this.logv2(scope, 'getChecksum', 'MobiKwik get checksum', {});
    let cellAndEmailText = '';
    if (requestParams.cell && requestParams.email) {
      cellAndEmailText = `'${requestParams.cell}''${requestParams.email}'`;
    } else if (requestParams.cell || requestParams.email) {
      cellAndEmailText = `'${requestParams.cell || requestParams.email}'`;
    } else {
      throw Errors.MobiKwik.InvalidRequest;
    }
    return {
      checksum: this._createChecksum(`${cellAndEmailText}'${requestParams.amount}''${requestParams.orderid}''${requestParams.redirectUrl}''${this.mobikwikConfig.merchantId}'`)
    };
  }

  responseFormatterForCheckUser(response) {
    let payLoad = {
      status: Enums.MobiKwik.ResponseStatus[response.status],
      message: _.get(response, 'statusdescription'),
      external_attributes: {
        statusdescription: _.get(response, 'statusdescription'),
        checksum: _.get(response, 'checksum')
      }
    }
    this.logv2(scope, 'responseFormatterForCheckUser', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);

  }

  responseFormatterForGenerateOTP(response) {
    let payLoad = {
      status: Enums.MobiKwik.ResponseStatus[response.status],
      message: _.get(response, 'statusdescription'),
      external_attributes: {
        statusdescription: _.get(response, 'statusdescription'),
        checksum: _.get(response, 'checksum')
      }
    }
    this.logv2(scope, 'responseFormatterForGenerateOTP', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);

  }

  responseFormatterForGenerateToken(response) {
    let payLoad = {
      status: Enums.MobiKwik.ResponseStatus[response.status],
      message: _.get(response, 'statusdescription'),
      external_attributes: {
        token: _.get(response, 'token'),
        checksum: _.get(response, 'checksum'),
        statusdescription: _.get(response, 'statusdescription')
      }
    }
    this.logv2(scope, 'responseFormatterForGenerateToken', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForCheckBalance(response) {
    let payLoad = {
      status: Enums.MobiKwik.ResponseStatus[response.status],
      message: _.get(response, 'statusdescription'),
      external_attributes: {
        balanceamount: _.get(response, 'balanceamount'),
        checksum: _.get(response, 'checksum'),
        statusdescription: _.get(response, 'statusdescription')
      }
    }
    this.logv2(scope, 'responseFormatterForCheckBalance', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForDebitWallet(response) {
    let payLoad = {
      status: Enums.MobiKwik.ResponseStatus[response.status],
      message: _.get(response, 'statusdescription'),
      external_attributes: {
        balanceamount: _.get(response, 'balanceamount'),
        checksum: _.get(response, 'checksum'),
        statusdescription: _.get(response, 'statusdescription'),
        debitedamount: _.get(response, 'debitedamount')
      }
    }
    this.logv2(scope, 'responseFormatterForDebitWallet', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForCheckStatus(response) {
    let payLoad = {
      status: response.statusmessage,
      message: response.message,
      external_attributes: {
        orderid: _.get(response, 'orderid'),
        checksum: _.get(response, 'checksum'),
        refid: _.get(response, 'refid'),
        amount: _.get(response, 'amount'),
        ordertype: _.get(response, 'ordertype')
      }
    }
    this.logv2(scope, 'responseFormatterForDebitWallet', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }
  responseFormatterForRefund(response) {
    let payLoad = {
      status: response.status,
      external_attributes: {
        refid: _.get(response, 'refid')
      }
    }
    if (payLoad.status === Constants.Status.transactionSuccess) {
      payLoad.message = response.status;
    }
    else {
      payLoad.message = response.statusmessage;
    }

    this.logv2(scope, 'responseFormatterForDebitWallet', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }
}
module.exports = MobikwikPayloadHelper;