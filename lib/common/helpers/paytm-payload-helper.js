'use strict'

const BaseHelper = require('base-lib').BaseHelper;
const repoInfo = require('../../../repo-info');
const scope = `PaytmPayloadHelper#${repoInfo.version}`;
const _ = require('lodash');
const Crypt = require('./checksum/crypt')
const PaytmConstants = require('../../payment-adapters/paytm/paytm-constants');
const ChecksumConstants = require('./checksum/checksum-constants');
const Constants = require('../constants');
const Joi = require('joi');

class PaytmPayloadHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.paytmConfig = config.paytm;
    this.crypt = new Crypt(dependencies, config, requestContext)
    this.className = `PaytmPayloadHelper#${repoInfo.version}`;
  }

  transformJsonData(schema, data) {
    this.logv2(scope, 'transformJsonData', 'transform JsonData', {});
    switch (schema) {
      case PaytmConstants.INITIATE_TRANSACTION:
        return this.prepareInitiateTransaction(data);
      case PaytmConstants.SEND_OTP:
        return this.prepareSendOTP(data);
      case PaytmConstants.VALIDATE_OTP:
        return this.prepareValidateOTP(data);
      case PaytmConstants.CHECK_BALANCE:
        return this.prepareCheckBalance(data)
      case PaytmConstants.PROCESS_TRANSACTION:
        return this.prepareProcessTransaction(data);
      default:
        return this.prepareInitiateTransaction(data);
    }
  }

  prepareInitiateTransaction(data) {
    let payload = this.getEmptyPayload();
    this.populateHeaders(payload);
    this.populateTransactionData(payload, data);
    this.populateOtherData(payload);
    this.populateUserInfo(payload, data)
    payload.head = _.omit(payload.head, ['txnToken']);
    this.logv2(scope, 'prepareInitiateTransaction', 'prepare Initiate Transaction', {});
    return payload;
  }

  populateHeaders(payload) {
    this.logv2(scope, 'populateHeaders', 'populate Headers', {});
    payload.head = {
      clientId: this.paytmConfig.client_id,
      version: this.paytmConfig.version,
      requestTimestamp: Date.now().toString(),
      channelId: this.paytmConfig.channel_id,
      signature: '',
      txnToken: ''
    };
  }


  populateUserInfo(payload, data) {
    this.logv2(scope, 'populateUserInfo', 'populate UserInfo', {});
    payload.body.userInfo = {
      custId: data.user_info.email,
      mobile: data.user_info.mobile,
      email: data.user_info.email,
      firstName: data.user_info.first_name,
      lastName: data.user_info.last_name
    }
  }

  populateTransactionData(payload, data) {
    this.logv2(scope, 'populateTransactionData', 'populate TransactionData', {});
    payload.body.orderId = data.transaction_context.transaction_id;
    payload.body.promoCode = '';
    payload.body.txnAmount = {
      currency: data.cart.currency,
      value: data.cart.total_price
    }
  }

  populateOtherData(payload) {
    this.logv2(scope, 'populateOtherData', 'populate OtherData', {});
    payload.body.mid = this.paytmConfig.merchant_id;
    payload.body.websiteName = this.paytmConfig.website_name;
    payload.body.callbackUrl = this.paytmConfig.callback_url
  }


  async createSignature(data) {
    this.logv2(scope, 'createSignature', 'create Signature', {});
    try {
      let bodyText = JSON.stringify(data);
      let salt = "";
      let signData = {};
      salt = await this.crypt.genSalt(4);
      let sha256 = this.crypt.generateSHA256(bodyText + ChecksumConstants.PIPE + salt);
      signData.checksum = this.crypt.encryptText(sha256 + salt, this.paytmConfig.key, this.paytmConfig.iv);
      return signData;
    } catch (e) {
      this.errorv2(PaytmPayloadHelper, 'createSignature', e);
      throw e;
    }
  }

  async createRefundSignature(refund) {
    this.logv2(scope, 'createRefundSignature', 'create Refund Signature', {});
    try {
      let refundText = this.formatToRefundText(refund);
      let salt = "";
      salt = await this.crypt.genSalt(4);
      let sha256 = this.crypt.generateSHA256(refundText + salt);
      let encrypted = this.crypt.encryptText(sha256 + salt, this.paytmConfig.key, this.paytmConfig.iv);
      refund.CHECKSUM = encodeURIComponent(encrypted);
      return refund;
    } catch (e) {
      this.errorv2(PaytmPayloadHelper, 'createRefundSignature', e);
      throw e;
    }
  }

  formatToRefundText(obj, mandatoryParams) {
    this.logv2(scope, 'formatToRefundText', 'format To RefundText', {});
    let data = "";
    let tempKeys = Object.keys(obj);
    tempKeys.sort();
    tempKeys.forEach(function (key) {
      let pipeKey = obj[key].includes(ChecksumConstants.PIPE);
      if (pipeKey == true) {
        obj[key] = "";
      }
      if (key !== ChecksumConstants.CHECKSUMHASH) {
        if (obj[key] === 'null') obj[key] = '';
        if (!mandatoryParams || mandatoryParams.indexOf(key) !== -1) {
          data += (obj[key] + ChecksumConstants.PIPE);
        }
      }
    });
    return data;
  }

  prepareSendOTP() {
    this.logv2(scope, 'prepareSendOTP', 'prepare request to Send OTP', {});
    let payload = this.getEmptyPayload();
    this.populateHeaders(payload);
    payload.head = _.omit(payload.head, ['signature']);
    return payload;
  }

  prepareValidateOTP() {
    this.logv2(scope, 'prepareValidateOTP', 'prepare request to Validate OTP', {});
    let payload = this.getEmptyPayload();
    this.populateHeaders(payload);
    payload.head = _.omit(payload.head, ['signature']);
    return payload;
  }

  prepareCheckBalance() {
    this.logv2(scope, 'prepareCheckBalance', 'prepare request to CheckBalance', {});
    let payload = this.getEmptyPayload();
    this.populateHeaders(payload);
    payload.head = _.omit(payload.head, ['signature'])
    return payload;
  }

  prepareProcessTransaction() {
    this.logv2(scope, 'prepareProcessTransaction', 'prepare request to ProcessTransaction', {});
    let payload = {
      data: {}
    };
    payload.data.mid = this.paytmConfig.merchant_id;
    payload.data.channelId = this.paytmConfig.channel_id;
    payload.submit_url = this.paytmConfig.submit_url;
    return payload;
  }

  prepareTransactionStatus() {
    this.logv2(scope, 'prepareTransactionStatus', 'prepare request to TransactionStatus', {});
    let payload = {
      MID: this.paytmConfig.merchant_id
    }
    return payload;
  }

  prepareRefund() {
    this.logv2(scope, 'prepareRefund', 'prepare request to Refund', {});
    let payload = {
      MID: this.paytmConfig.merchant_id
    }
    return payload;
  }

  getEmptyPayload() {
    return {
      head: {},
      body: {}
    };
  }

  validateRequest(requestBody, schema) {
    this.logv2(scope, 'validateRequest', 'validate the Request', {});
    let response = Joi.validate(requestBody, schema);
    if (response.error) {
      this.logv2(this.className, 'validateRequest', `The Joi Validation of the request `, { response });
      return false;
    }
    return true;
  }

  formatResponse(response, toWhat) {
    this.logv2(scope, 'formatResponse', 'format the Response', {});
    let responsePayload = {
      external_attributes: {}
    }
    let status = _.get(response, PaytmConstants.PAYTM_RESPONSES.STATUS_PATH);
    let message = _.get(response, PaytmConstants.PAYTM_RESPONSES.MESSAGE_PATH);
    let resultCode = _.get(response, PaytmConstants.PAYTM_RESPONSES.RESULT_CODE);
    responsePayload.message = message;
    responsePayload.status = Constants.Status.Pending;
    switch (toWhat) {
      case PaytmConstants.INITIATE_TRANSACTION:
        if (status && status.toUpperCase() !== PaytmConstants.PAYTM_RESPONSES.S.toUpperCase()) {
          throw new Error(message);
        }
        responsePayload.external_attributes.txn_token = _.get(response, PaytmConstants.PAYTM_RESPONSES.TOKEN_PATH);
        break;
      case PaytmConstants.SEND_OTP:
        if (status && status.toUpperCase() !== PaytmConstants.SUCCESS.toUpperCase()) {
          throw new Error(message);
        }
        break;
      case PaytmConstants.VALIDATE_OTP:
        if (status && status.toUpperCase() === PaytmConstants.FAILURE.toUpperCase() && resultCode !== "403") {
          throw new Error(message);
        }
        break;
    }
    return responsePayload;
  }

  formatTransactionStatusResponse(response) {
    this.logv2(scope, 'formatTransactionStatusResponse', 'format TransactionStatus Response', {});
    let responsePayload = {
      external_attributes: {}
    };
    let errorCode = _.get(response, "ErrorCode");
    let errorMsg = _.get(response, "ErrorMsg");
    if (_.isEmpty(errorCode) && _.isEmpty(errorMsg)) {
      if (response.STATUS && response.STATUS === PaytmConstants.PAYTM_RESPONSES.SUCCESS) {
        responsePayload.status = Constants.Status.Success;
        responsePayload.message = response.RESPMSG;
        responsePayload.external_attributes.pg_txn_date = response.TXNDATE;
        responsePayload.external_attributes.pg_txn_amount = response.TXNAMOUNT;
        responsePayload.external_attributes.pg_transaction_id = response.TXNID;
      } else {
        responsePayload.status = Constants.Status.Failure;
        responsePayload.message = response.RESPMSG;
      }
      return _.omitBy(responsePayload, _.isUndefined);
    }
    else {
      throw new Error(errorMsg);
    }
  }

  setResponseKey(responsePayload, response, toWhat) {
    this.logv2(scope, 'setResponseKey', 'set the Response Key', {});
    switch (toWhat) {
      case PaytmConstants.INITIATE_TRANSACTION:
        responsePayload.txn_token = _.get(response, PaytmConstants.PAYTM_RESPONSES.TOKEN_PATH);
        break;
      case PaytmConstants.VALIDATE_OTP:
        responsePayload.validate_otp = _.get(response, PaytmConstants.PAYTM_RESPONSES.AUTHENTICATED_PATH);
        break;
      default:
        break;
    }
  }


  removeExtraData(response) {

    return _.omit(response, [PaytmConstants.PAYTM_RESPONSES.HEAD_PATH, PaytmConstants.PAYTM_RESPONSES.RESULT_INFO_PATH]);
  }

}
module.exports = PaytmPayloadHelper;