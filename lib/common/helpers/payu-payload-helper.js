'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  Constants = require('../constants'),
  repoInfo = require('../../../repo-info'),
  scope = `PayUPayloadHelper#${repoInfo.version}`,
  Enums = require('../enum'),
  crypto = require('crypto'),
  _ = require('lodash'),
  Errors = require('../../errors/error');

class PayUPayloadHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  startTransactionHelper(inputPayload, options = {}) {
    let payLoad = {
      txnid: _.get(inputPayload, 'transaction_context.transaction_id'),
      amount: _.get(inputPayload, 'cart.total_price'),
      productinfo: _.map(_.get(inputPayload, 'cart.line_items'), 'sku_id').join(','),
      firstname: _.get(inputPayload, 'user_info.first_name'),
      email: _.get(inputPayload, 'user_info.email'),
      phone: _.get(inputPayload, 'user_info.mobile'),
      udf1: _.get(inputPayload, 'external_attributes.udf1'),
      udf2: _.get(inputPayload, 'external_attributes.udf2'),
      udf3: _.get(inputPayload, 'external_attributes.udf3'),
      udf4: _.get(inputPayload, 'external_attributes.udf4'),
      udf5: _.get(inputPayload, 'external_attributes.udf5'),
      udf6: _.get(inputPayload, 'external_attributes.udf6'),
      udf7: _.get(inputPayload, 'external_attributes.udf7'),
      udf8: _.get(inputPayload, 'external_attributes.udf8'),
      udf9: _.get(inputPayload, 'external_attributes.udf9'),
      pg: Enums.PayU.PaymentMethods[_.get(inputPayload, 'instrument')],
      ccnum: _.get(inputPayload, 'payment_info.card_number'),
      ccname: _.get(inputPayload, 'payment_info.name'),
      ccvv: _.get(inputPayload, 'payment_info.cvv'),
      ccexpmon: _.get(inputPayload, 'payment_info.expiration_month'),
      ccexpyr: _.get(inputPayload, 'payment_info.expiration_year'),
      bankcode: _.get(inputPayload, 'payment_option.code'),
      surl: _.get(inputPayload, 'external_attributes.success_url') || this.config.payu.success_url,
      furl: _.get(inputPayload, 'external_attributes.failure_url') || this.config.payu.failure_url,
      enforce_paymethod: _.get(inputPayload, 'external_attributes.enforce_paymethod'),
      user_credentials: _.get(inputPayload, 'auth_info.billing_record_id'),
      store_card_token: _.get(inputPayload, 'external_attributes.card_token'),
      store_card: _.get(inputPayload, 'auth_info.save_card')
    };
    let offerApplied = _.get(inputPayload, 'cart.pg_offers');
    if (offerApplied) {
      payLoad.offer_key = _.map(offerApplied, 'external_id').join(",")
    }
    if(_.get(inputPayload, 'instrument') == Constants.PayU.UPI) {
      payLoad.vpa = _.get(inputPayload, 'user_info.email')
    }
    let hash = this._createHash(this._createHashTextForTransactionRequest(payLoad));
    payLoad = _.merge(payLoad, options, {
      hash: hash,
      txn_s2s_flow: Constants.PayU.TransactionS2SFlow,
      key: this.config.payu.key
    });
    this.logv2(scope, 'getPayLoadForInitiateTransaction', 'Constructing payload', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  OTPRequestHelper(inputPayload, options = {}) {
    let payLoad = {
      payLoad: {
        referenceId: _.get(inputPayload, 'external_attributes.reference_id'),
        otp: _.get(inputPayload, 'auth_info.otp'),
        resendOtp: _.get(inputPayload, 'resend_otp')
      }
    };
    const text = [this.config.payu.key, Constants.PayU.PureS2S, payLoad.payLoad.referenceId, this.config.payu.salt].join('|');
    payLoad.otpRequestURL = _.get(inputPayload, 'external_attributes.otp_request_url')
    payLoad.payLoad = _.merge(payLoad.payLoad, options, {
      data: Constants.PayU.SubmitOTPData,
      hash: this._createHash(text)
    });
    this.logv2(scope, 'OTPRequestHelper', 'Constructing payload for OTP submission', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  getRedirectPayload(inputPayload, options = {}) {
    let payLoad = {
      submit_url: this.config.payu.serviceUrl,
      data: {
        key: this.config.payu.key,
        txnid: _.get(inputPayload, 'transaction_context.transaction_id'),
        amount: _.get(inputPayload, 'cart.total_price'),
        productinfo: _.map(_.get(inputPayload, 'cart.line_items'), 'sku_id').join(','),
        firstname: _.get(inputPayload, 'user_info.first_name'),
        email: _.get(inputPayload, 'user_info.email'),
        phone: _.get(inputPayload, 'user_info.mobile'),
        udf1: _.get(inputPayload, 'external_attributes.udf1'),
        udf2: _.get(inputPayload, 'external_attributes.udf2'),
        udf3: _.get(inputPayload, 'external_attributes.udf3'),
        udf4: _.get(inputPayload, 'external_attributes.udf4'),
        udf5: _.get(inputPayload, 'external_attributes.udf5'),
        udf6: _.get(inputPayload, 'external_attributes.udf6'),
        udf7: _.get(inputPayload, 'external_attributes.udf7'),
        udf8: _.get(inputPayload, 'external_attributes.udf8'),
        udf9: _.get(inputPayload, 'external_attributes.udf9'),
        pg: Enums.PayU.PaymentMethods[_.get(inputPayload, 'instrument')],
        enforce_paymethod: inputPayload.enforce_paymethod,
        surl: _.get(inputPayload, 'external_attributes.success_url') || this.config.payu.success_url,
        furl: _.get(inputPayload, 'external_attributes.failure_url') || this.config.payu.failure_url,
        bankcode: _.get(inputPayload, 'payment_option.code'),
        user_credentials: _.get(inputPayload, 'auth_info.billing_record_id'),
        store_card_token: _.get(inputPayload, 'external_attributes.card_token')
      }
    };
    let offerApplied = _.get(inputPayload, 'cart.pg_offers');
    if (offerApplied) {
      payLoad.data.offer_key = _.map(offerApplied, 'external_id').join(",")
    }
    let hash = this._createHash(this._createHashTextForTransactionRequest(payLoad.data));
    payLoad.data = _.merge(payLoad.data, options, {
      hash: hash
    });
    this.logv2(scope, 'getRedirectPayload', 'Constructing payload for redirect API', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  getTransactionStatusHelper(inputPayload, options) {
    let payLoad = {
      key: this.config.payu.key,
      command: Constants.PayU.VerifyPayment,
      var1: _.get(inputPayload, 'transaction_context.transaction_id')
    };
    const text = [this.config.payu.key, Constants.PayU.VerifyPayment, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      command: Constants.PayU.VerifyPayment,
      hash: this._createHash(text)
    });
    this.logv2(scope, 'getTransactionStatusHelper', 'Constructing payload for Get Transactions API', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  checkOfferStatusHelper(inputPayload, options) {
    let payLoad = {
      key: this.config.payu.key,
      command: Constants.PayU.CheckOfferStatus,
      var1: _.get(inputPayload, 'cart.pg_offers[0].external_id'),
      var2: _.get(inputPayload, 'cart.total_price'),
      var3: options.cardCategory,
      var4: options.issuingBank,
      var5: _.get(inputPayload, 'payment_info.card_number'),
      var6: _.get(inputPayload, 'payment_info.name'),
      var7: _.get(inputPayload, 'user_info.mobile'),
      var8: _.get(inputPayload, 'user_info.email')
    };
    const text = [this.config.payu.key, Constants.PayU.CheckOfferStatus, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      hash: this._createHash(text)
    });
    this.logv2(scope, 'checkOfferStatusHelper', 'Constructing payload for offer check', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  initiateRefundHelper(inputPayload, options = {}) {
    let payLoad = {
      key: this.config.payu.key,
      command: Constants.PayU.InitiateRefund,
      var1: _.get(inputPayload, 'external_attributes.transaction_id'),
      var2: Date.now()+""+Math.floor(Math.random()*10000+1),
      var3: _.get(inputPayload, 'cart.total_price')
    };
    const text = [this.config.payu.key, Constants.PayU.InitiateRefund, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      hash: this._createHash(text)
    });
    this.logv2(scope, 'initiateRefundHelper', 'Constructing payload for initiate refund API', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  checkIsDomesticHelper(inputPayload, options = {}) {
    let payLoad = {
      key: this.config.payu.key,
      command: Constants.PayU.CheckIsDomestic,
      var1: _.get(inputPayload, 'payment_info.card_number')
    };
    const text = [this.config.payu.key, Constants.PayU.CheckIsDomestic, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      hash: this._createHash(text)
    });
    this.logv2(scope, 'checkIsDomesticHelperHelper', 'Constructing payload for check is domestic API', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  validateVPAHelper(inputPayload, options = {}) {
    let payLoad = {
      key: this.config.payu.key,
      var1: _.get(inputPayload, 'vpa')
    };
    const text = [this.config.payu.key, Constants.PayU.ValidateVPA, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      command: Constants.PayU.ValidateVPA,
      hash: this._createHash(text)
    });
    this.logv2(scope, 'validateVPAHelper', 'Constructing payload for validateVPA API', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  checkUPITxnStatusHelper(inputPayload, options = {}) {
    let payLoad = {
      key: this.config.payu.key,
      var1: _.get(inputPayload, 'transaction_context.transaction_id')
    };
    const text = [this.config.payu.key, Constants.PayU.CheckUPITxnStatus, payLoad.var1, this.config.payu.salt].join('|');
    payLoad = _.merge(payLoad, options, {
      command: Constants.PayU.CheckUPITxnStatus,
      hash: this._createHash(text)
    });
    this.logv2(scope, 'checkUPITxnStatusHelper', 'Constructing payload for checkUPITxnStatusHelper API', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatter(response) {
    if(response.status != "success"){
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'message')}
    }
    let payLoad = {
      status: Enums.PayU.ResponseStatus[response.status],
      message: response.message,
      external_attributes: {
        otp_request_url: _.get(response, 'result.post_uri'),
        reference_id: _.get(response, 'result.referenceId')
      }
    }
    this.logv2(scope, 'responseFormatter', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForOTPSubmission(response) {
    if(_.get(response, 'result.status') != "success"){
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'result.error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'result.error_Message')}
    }
    let payLoad = {
      status: Enums.PayU.ResponseStatus[_.get(response, 'result.status')] || Enums.PayU.ResponseStatus[response.status],
      message: _.get(response, 'result.error_Message'),
      external_attributes: {
        transaction_id: _.get(response, 'result.mihpayid'),
        card_token: _.get(response, 'result.card_token'),
        card_no: _.get(response, 'result.card_no'),
        net_amount_debit: _.get(response, 'result.net_amount_debit'),
        bank_ref_no: _.get(response, 'result.bank_ref_no'),
        bank_ref_num: _.get(response, 'result.bank_ref_num'),
        discount: _.get(response, 'result.discount'),
        offer: _.get(response, 'result.offer'),
        offer_type: _.get(response, 'result.offer_type'),
        offer_availed: _.get(response, 'result.offer_availed'),
        offer_failure_reason: _.get(response, 'result.offer_failure_reason')
      }
    }
    this.logv2(scope, 'responseFormatterForOTPSubmission', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForOTPResend(response) {
    if(response.status != "success"){
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'result.message')}
    }
    let payLoad = {
      status: Enums.PayU.ResponseStatus[response.status],
      message: _.get(response, 'result.message') || _.get(response, 'msg'),
      external_attributes: {
        resend_otp_attempt_left: _.get(response, 'resendOtpAttemptLeft')
      }
    }
    this.logv2(scope, 'responseFormatterForOTPResend', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForTransactionStatus(response) {
    let status = '';
    let message = '';
    if (response.status != Constants.Status.ONE) {
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'msg')}
    } else if (!(_.isEmpty(response.transaction_details)) && Object.keys(response.transaction_details).length > 0) {
      let payuStatus = response.transaction_details[Object.keys(response.transaction_details)[0]].status;
      status = Constants.Status[payuStatus];
      message = response.transaction_details[Object.keys(response.transaction_details)[0]].error_Message
    } else {
      status = Constants.Status.Failed;
    }

    let payLoad = {
      status: status,
      message: message || _.get(response, 'msg'),
      external_attributes: {
        transaction_details: _.get(response, 'transaction_details')
      }
    }
    this.logv2(scope, 'responseFormatterForTransactionStatus', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForOfferCheck(response) {
    let payLoad = {
      status: response.status ? Constants.Status.Valid : Constants.Status.Invalid,
      message: _.get(response, 'msg'),
      external_attributes: {
        offer_key: _.get(response, 'offer_key'),
        offer_type: _.get(response, 'offer_type'),
        offer_availed_count: _.get(response, 'offer_availed_count'),
        offer_remaining_count: _.get(response, 'offer_remaining_count'),
        category: _.get(response, 'category'),
        discount: _.get(response, 'discount')
      }
    }
    this.logv2(scope, 'responseFormatterForOfferCheck', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForInitiateRefund(response) {
    if(response.status != Constants.Status.ONE){
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'msg')}
    }
    let payLoad = {
      status: _.get(response, 'status'),
      message: _.get(response, 'msg'),
      external_attributes: {
        txn_update_id: _.get(response, 'txn_update_id'),
        bank_ref_num: _.get(response, 'bank_ref_num'),
        mihpayid: _.get(response, 'mihpayid'),
        error_code: _.get(response, 'error_code')
      }
    }
    this.logv2(scope, 'responseFormatterForOfferCheck', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForCheckUPITxnStatus(response) {
    if(response.status != Constants.Status.ONE){
      throw { errorCode: Errors.ErrorCodes.PayU[_.get(response, 'error')] || Constants.StatusCode.NotAcceptable, message: _.get(response, 'msg')}
    }
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'msg'),
      external_attributes: {
        result: _.get(response, 'result')
      }
    }
    this.logv2(scope, 'responseFormatterForCheckUPITxnStatus', 'Constructing response', { payLoad });
    return _.omitBy(payLoad, _.isUndefined);
  }

  _createHashTextForTransactionRequest(inputParams) {
    const text = [this.config.payu.key, inputParams.txnid, inputParams.amount, inputParams.productinfo, inputParams.firstname, inputParams.email,
      inputParams.udf1, inputParams.udf2, inputParams.udf3, inputParams.udf4, inputParams.udf5, inputParams.udf6, inputParams.udf7, inputParams.udf8,
      inputParams.udf9, inputParams.udf10, this.config.payu.salt].join('|');
    return text;
  }

  _createHashTextForTransactionResponse(inputParams) {
    const text = [this.config.payu.salt + inputParams.status, inputParams.udf10, inputParams.udf9, inputParams.udf8, inputParams.udf7, inputParams.udf6,
      inputParams.udf5, inputParams.udf4, inputParams.udf3, inputParams.udf2, inputParams.udf1, inputParams.email, inputParams.firstname, inputParams.productinfo,
      inputParams.amount, inputParams.txnid, this.config.payu.key].join('|');
    return text;
  }

  _createHash(text) {
    this.logv2(scope, '_createHash', 'PayU create hash', {});
    return crypto.createHash('sha512').update(text, 'utf8').digest('hex');
  }

}

module.exports = PayUPayloadHelper;