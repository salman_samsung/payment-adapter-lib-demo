'use strict'

const BaseHelper = require('base-lib').BaseHelper;
const repoInfo = require('../../../repo-info');
const scope = `PinelabsHelper#${repoInfo.version}`;
const Crypt = require('./checksum/crypt');
const ChecksumConstants = require('./checksum/checksum-constants')
const PinelabsConstants = require('../../payment-adapters/pinelabs/pinelabs-constants');
const Constants = require('../constants');
const _ = require('lodash');
const Joi = require('joi');

class PinelabsHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.pinelabsConfig = config.pinelabs;
    this.crypt = new Crypt(dependencies, config, requestContext);
    this.className = `PinelabsHelper#${repoInfo.version}`;
  }


  prepareBrandEMIRequest(data) {
    this.logv2(scope, 'prepareBrandEMIRequest', 'prepare the BrandEMI Request', {});
    let payload = {};
    payload.MERCHANT_ACCESS_CODE = this.pinelabsConfig.merchant_access_code;
    payload.MERCHANT_ID = this.pinelabsConfig.merchant_id;
    payload.AMOUNT = data.cart.total_price * 100;
    payload.PRODUCT_CODE = _.get(data, 'cart.line_items[0].sku_id');
    return payload;
  }

  prepareValidateSchemaRequest(data) {
    this.logv2(scope, 'prepareValidateSchemaRequest', 'prepare the Request to ValidateSchema', {});
    let payload = {};
    let financeScheme = _.get(data, 'finance_plan');
    let schema = [{ PROGRAM_TYPE: _.get(financeScheme, 'scheme.program_type'), SCHEME_ID: _.get(financeScheme, 'scheme.scheme_id') }];
    payload.CARD_NUMBER = this.crypt.encryptText(data.payment_info.card_number, this.pinelabsConfig.enc_key, this.pinelabsConfig.iv);
    payload.MERCHANT_ACCESS_CODE = this.pinelabsConfig.merchant_access_code;
    payload.MERCHANT_ID = this.pinelabsConfig.merchant_id;
    payload.AMOUNT = (_.get(data, 'cart.total_price') * 100).toString();
    payload.PRODUCT_CODE = _.get(data, 'cart.line_items[0].sku_id');
    payload.SCHEME = JSON.stringify(schema);
    payload.TENURE_ID = _.get(financeScheme, 'tenure.value');
    payload.KEY_ID = this.pinelabsConfig.key_id;
    return payload;
  }

  prepareEMIPurchaseRequest(data) {
    this.logv2(scope, 'prepareEMIPurchaseRequest', 'prepare the BEMIPurchase Request', {});
    let payload = {
      submit_url: this.pinelabsConfig.submit_url,
      data: {}
    }
    let financeScheme = _.get(data, 'finance_plan');
    let cc_text = this.FormatCardToText(data.payment_info);
    let payCredentials = this.crypt.encryptText(cc_text, this.pinelabsConfig.enc_key, this.pinelabsConfig.iv);
    payload.data.ppc_UniqueMerchantTxnID = data.transaction_context.transaction_id;
    payload.data.ppc_Amount = (data.cart.total_price * 100).toString();
    if (financeScheme.discount_amount) {
      payload.data.ppc_Amount = financeScheme.amount_after_discount ? (parseFloat(financeScheme.amount_after_discount) * 100).toString() : "";
      payload.data.ppc_OriginalTxnAmt = (data.cart.total_price * 100).toString();
      payload.data.ppc_InstantDiscountAmt = financeScheme.discount_amount ? (parseFloat(financeScheme.discount_amount) * 100).toString() : "";
    }
    payload.data.ppc_MerchantID = this.pinelabsConfig.merchant_id;
    payload.data.ppc_MerchantAccessCode = this.pinelabsConfig.merchant_access_code;
    payload.data.ppc_NavigationMode = PinelabsConstants.NAVIGATION_MODE.SEAMLESS;
    payload.data.ppc_TransactionType = PinelabsConstants.TRANSACTION_TYPE.PURCHASE;
    payload.data.ppc_LPC_SEQ = this.pinelabsConfig.lpc_sec;
    payload.data.ppc_PayModeOnLandingPage = PinelabsConstants.LANDING_PAGE.EMI;
    let schema = [{ PROGRAM_TYPE: financeScheme.scheme.program_type, SCHEME_ID: financeScheme.scheme.scheme_id }];
    payload.data.ppc_Scheme = JSON.stringify(schema);
    payload.data.ppc_TenureID = _.get(financeScheme, 'tenure.value');
    payload.data.ppc_Product_Code = _.get(data, 'cart.line_items[0].sku_id');
    payload.data.ppc_CurrencyCode = this.pinelabsConfig.currency_code;
    payload.data.ppc_MerchantReturnURL = _.get(data, 'external_attributes.merchant_url') || this.pinelabsConfig.merchant_url;
    payload.data.ppc_PayCredentials = payCredentials;
    payload.data.ppc_KeyID = this.pinelabsConfig.key_id
    let signature = this.getSignature(payload.data);
    payload.data.ppc_DIA_SECRET = signature;
    payload.data.ppc_DIA_SECRET_TYPE = ChecksumConstants.SHA256;
    return payload;
  }

  prepareInquiryRequest(inquiryRequest) {
    this.logv2(scope, 'prepareInquiryRequest', 'prepare the Inquiry Request', {});
    let payload = {};
    payload.ppc_Amount = (_.get(inquiryRequest, 'cart.total_price') * 100).toString();
    payload.ppc_MerchantAccessCode = this.pinelabsConfig.merchant_access_code;
    payload.ppc_MerchantID = this.pinelabsConfig.merchant_id;
    payload.ppc_TransactionType = PinelabsConstants.TRANSACTION_TYPE.INQUIRY;
    payload.ppc_CurrencyCode = this.pinelabsConfig.currency_code;
    payload.ppc_UniqueMerchantTxnID = inquiryRequest.transaction_context.transaction_id;
    let signature = this.getSignature(payload);
    payload.ppc_DIA_SECRET = signature;
    payload.ppc_DIA_SECRET_TYPE = ChecksumConstants.SHA256;
    return payload;
  }

  FormatToText(obj) {
    this.logv2(scope, 'FormatToText', 'FormatToText', {});
    let keys = Object.keys(obj);
    let data = [];
    keys.sort();
    keys.forEach(function (key) {
      let tempData = key + "=" + obj[key];
      data.push(tempData)
    })
    return data.join("&");

  }

  getSignature(payload) {
    this.logv2(scope, 'getSignature', 'getSignature', {});
    let text_to_hash = this.FormatToText(payload);
    let byteArr = this.crypt.convertHexToByteArray(this.pinelabsConfig.key);
    return this.crypt.createSignature(text_to_hash, byteArr, ChecksumConstants.SHA256, true);
  }

  FormatCardToText(obj) {
    this.logv2(scope, 'FormatCardToText', 'FormatCardToText', {});
    return `${obj.card_number}|${obj.cvv}|${obj.expiration_month.toLocaleString(undefined, { minimumIntegerDigits: 2 })}${obj.expiration_year}|${obj.name}`;
  }

  validateInquiryResponse(data) {
    this.logv2(scope, 'validateInquiryResponse', 'validate Inquiry Response', {});
    let actual_hash = data.ppc_DIA_SECRET;
    let payload = _.omit(data, ['ppc_DIA_SECRET', 'ppc_DIA_SECRET_TYPE']);
    let generated_hash = this.getSignature(payload);
    if (actual_hash === generated_hash)
      return true;
    else
      return false;
  }

  formatResponse(response) {
    this.logv2(scope, 'formatResponse', 'format the Response', {});
    let responsePayload = {}
    responsePayload.status = Constants.Status.Success;
    responsePayload.message = "";
    if (response.RESPONSE_CODE !== Constants.ResponseCode.One) {
      responsePayload.status = Constants.Status.Failure;
      responsePayload.message = response.RESPONSE_MESSAGE;
    }
    return responsePayload;
  }

  formatInquiryResponse(response) {
    this.logv2(scope, 'formatInquiryResponse', 'format Inquiry Response', {});
    let responsePayload = {
      external_attributes: {}
    };
    responsePayload.status = Constants.Status.Success;
    if (response.ppc_PinePGTxnStatus && response.ppc_PinePGTxnStatus === PinelabsConstants.RESPONSE_CODE.CODE7) {
      switch (response.ppc_Parent_TxnStatus) {
        case PinelabsConstants.RESPONSE_CODE.CODE4:
          responsePayload.status = Constants.Status.Success;
          break;
        case PinelabsConstants.RESPONSE_CODE.CODE_7:
          responsePayload.status = Constants.Status.Failure;
          break;
        case PinelabsConstants.RESPONSE_CODE.CODE_10:
          responsePayload.status = Constants.Status.Cancelled
          break;
        case PinelabsConstants.RESPONSE_CODE.CODE1:
          responsePayload.status = Constants.Status.Initiated
          break;
      }
    } else {
      throw new Error(response.ppc_TxnResponseMessage);
    }
    responsePayload.message = response.ppc_ParentTxnResponseMessage || response.ppc_TxnResponseMessage;
    responsePayload.external_attributes.pg_transaction_id = response.ppc_PinePGTransactionID;
    responsePayload.external_attributes.pg_txn_date = response.ppc_TransactionCompletionDateTime;
    responsePayload.external_attributes.pg_txn_amount = response.ppc_Amount;
    responsePayload.external_attributes.pg_product_code = response.ppc_ProductCode;
    responsePayload.external_attributes.pg_emi_amount = response.ppc_EMIAmountPayableEachMonth;
    responsePayload.external_attributes.pg_emi_interest_rate_percent = response.ppc_EMIInterestRatePercent;
    responsePayload.external_attributes.pg_emi_tenure_month = response.ppc_EMITenureMonth;
    return _.omitBy(responsePayload, _.isUndefined);
  }

  formatlistBrandEMISResponse(response) {
    let responsePayload = {
      external_attributes: {}
    };
    responsePayload.status = Constants.Status.Success;
    responsePayload.message = "";
    if (response.RESPONSE_CODE !== Constants.ResponseCode.One) {
      responsePayload.status = Constants.Status.Failure;
      responsePayload.message = response.RESPONSE_MESSAGE;
    }
    responsePayload.external_attributes.issuer = response.ISSUER;
    return _.omitBy(responsePayload, _.isUndefined);
  }

  validateRequest(requestBody, schema) {
    this.logv2(scope, 'validateRequest', 'validate Request', {});
    let response = Joi.validate(requestBody, schema);
    if (response.error) {
      this.logv2(this.className, 'validateRequest', `The Joi Validation of the request `, { response });
      return false;
    }
    return true;
  }


}
module.exports = PinelabsHelper;