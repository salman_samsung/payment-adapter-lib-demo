'use strict'

const BaseHelper = require('base-lib').BaseHelper,
  repoInfo = require('../../../repo-info'),
  _ = require('lodash'),
  scope = `QwikcilverHelper#${repoInfo.version}`,
  Constants = require('../constants'),
  moment = require('moment');

class QwikcilverHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.qwikcilverConfig = config.qwikcilver;
  }

  prepareInitializeHeaders(){
    this.logv2(scope, 'prepareInitializeHeaders', 'prepare initialize Headers', {});
    let headers = {};
    headers.TerminalId = this.qwikcilverConfig.terminal_id;
    headers.UserName = this.qwikcilverConfig.username;
    headers.Password = this.qwikcilverConfig.password;
    headers.ForwardEntityId = this.qwikcilverConfig.forward_entity_id;
    headers.ForwardEntityPassword = this.qwikcilverConfig.forward_entity_password;
    headers.TermAppVersion = this.qwikcilverConfig.term_app_version;
    headers.DateAtClient = moment(new Date()).format('YYYY-MM-DD HH:mm');
    headers.TransactionId = Math.floor((Math.random()*999999999) + 1);
    return headers;
  }

  prepareBalanceEnquiryOrRedeemHeaders(inputPayload){
    this.logv2(scope, 'prepareBalanceEnquiryOrRedeemHeaders', 'prepare balance enquiry or redeem headers', {});
    let headers = {};
    headers.TerminalId = this.qwikcilverConfig.terminal_id;
    headers.UserName = this.qwikcilverConfig.username;
    headers.Password = this.qwikcilverConfig.password;
    headers.ForwardEntityId = this.qwikcilverConfig.forward_entity_id;
    headers.ForwardEntityPassword = this.qwikcilverConfig.forward_entity_password;
    headers.TermAppVersion = this.qwikcilverConfig.term_app_version;
    headers.DateAtClient = moment(new Date()).format('YYYY-MM-DD HH:mm');
    headers.TransactionId = Math.floor((Math.random()*999999999) + 1);
    headers.MerchantOutletName = this.qwikcilverConfig.merchant_outlet_name;
    headers.AcquirerId = this.qwikcilverConfig.acquirer_id;
    headers.OrganizationName = this.qwikcilverConfig.organization_name;
    headers.POSEntryMode = this.qwikcilverConfig.pos_entry_mode;
    headers.POSTypeId = this.qwikcilverConfig.pos_type_id;
    headers.POSName = this.qwikcilverConfig.pos_name;
    headers.CurrentBatchNumber = _.get(inputPayload, Constants.QwikCilver.Paths.BatchNumber);
    return headers;
  }

  prepareBalanceEnquiryRequest(inputPayload){
    this.logv2(scope, 'prepareBalanceEnquiryRequest', 'prepare balance enquiry request', {});
    let payload = {};
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    return payload;
  }

  prepareRedeemValidationRequest(inputPayload){
    this.logv2(scope, 'prepareRedeemValidationRequest', 'prepare redeem validation request', {});
    let payload = {};
    let amount = this.getRedeemAmount(_.get(inputPayload, Constants.QwikCilver.Paths.Amount), _.get(inputPayload, Constants.QwikCilver.Paths.CardAmount));
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    payload.Notes = `{${Constants.QwikCilver.Notes.VldType}~${Constants.QwikCilver.Notes.EGCRDM}|${Constants.QwikCilver.Notes.Amount}~
      ${amount}|${Constants.QwikCilver.Notes.NetBillValue}~${amount}|${Constants.QwikCilver.Notes.POSType}~${Constants.QwikCilver.Notes.Magento}}`;
    return payload;
  }

  prepareSendOTPRequest(inputPayload){
    this.logv2(scope, 'prepareSendOTPRequest', 'prepare send OTP request', {});
    let payload = {};
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    return payload;
  }

  prepareSoftRedeemRequest(inputPayload){
    this.logv2(scope, 'prepareSoftRedeemRequest', 'prepare soft redeem request', {});
    let payload = {};
    let amount = this.getRedeemAmount(_.get(inputPayload, Constants.QwikCilver.Paths.Amount), _.get(inputPayload, Constants.QwikCilver.Paths.CardAmount));
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    payload.CardPIN = _.get(inputPayload, 'auth_info.otp');
    payload.Notes = `{${Constants.QwikCilver.Notes.VldType}~${Constants.QwikCilver.Notes.EGCSRDM}|${Constants.QwikCilver.Notes.Amount}~
      ${amount}|${Constants.QwikCilver.Notes.NetBillValue}~${amount}|${Constants.QwikCilver.Notes.POSType}~${Constants.QwikCilver.Notes.Magento}}`;
    return payload;
  }

  prepareHardRedeemRequest(inputPayload){
    this.logv2(scope, 'prepareHardRedeemRequest', 'prepare hard redeem request', {});
    let payload = {};
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    payload.Amount = this.getRedeemAmount(_.get(inputPayload, Constants.QwikCilver.Paths.Amount), _.get(inputPayload, Constants.QwikCilver.Paths.CardAmount));
    payload.BillAmount = this.getRedeemAmount(_.get(inputPayload, Constants.QwikCilver.Paths.Amount), _.get(inputPayload, Constants.QwikCilver.Paths.CardAmount));
    payload.InvoiceNumber = Date.now();
    payload.Notes = `${Constants.QwikCilver.Notes.Paymode}~${Constants.QwikCilver.Notes.EGC}`;
    payload.IdempotencyKey = Date.now();
    return payload;
  }

  prepareCancelRedeemRequest(inputPayload){
    this.logv2(scope, 'prepareCancelRedeemRequest', 'prepare cancel redeem request', {});
    let payload = {};
    payload.CardNumber = _.get(inputPayload, Constants.QwikCilver.Paths.CardNumber);
    payload.OriginalAmount = _.get(inputPayload, 'external_attributes.bill_amount');
    payload.OriginalInvoiceNumber = _.get(inputPayload, 'external_attributes.invoice_number');
    payload.OriginalTransactionId = _.get(inputPayload, 'external_attributes.transaction_id');
    payload.OriginalBatchNumber = _.get(inputPayload, Constants.QwikCilver.Paths.BatchNumber);
    payload.OriginalApprovalCode = _.get(inputPayload, 'external_attributes.approval_code');
    payload.IdempotencyKey = Date.now();
    return payload;
  }

  responseFormatterForInitialize(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber')
      }
    }
    this.logv2(scope, 'responseFormatterForInitialize', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForBalanceEnquiry(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        amount: _.get(response, 'Amount')
      }
    }
    this.logv2(scope, 'responseFormatterForBalanceEnquiry', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForRedeemValidation(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        amount: _.get(response, 'Amount')
      }
    }
    this.logv2(scope, 'responseFormatterForRedeemValidation', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForSendOTP(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        amount: _.get(response, 'Amount'),
        card_pin: _.get(response, 'CardPIN')
      }
    }
    this.logv2(scope, 'responseFormatterForSendOTP', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForSoftRedeem(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        amount: _.get(response, 'Amount')
      }
    }
    this.logv2(scope, 'responseFormatterForSoftRedeem', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForHardRedeem(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        balance_amount : _.get(response, 'Amount'),
        approval_code : _.get(response, 'ApprovalCode'),
        invoice_number : _.get(response, 'InvoiceNumber'),
        bill_amount : _.get(response, 'BillAmount')
      }
    }
    this.logv2(scope, 'responseFormatterForHardRedeem', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  responseFormatterForCancelRedeem(response) {
    let payLoad = {
      status: Constants.Status.Success,
      message: _.get(response, 'ResponseMessage'),
      external_attributes: {
        transaction_id: _.get(response, 'TransactionId'),
        batch_number: _.get(response, 'ApiWebProperties.CurrentBatchNumber'),
        balance_amount : _.get(response, 'Amount'),
        approval_code : _.get(response, 'ApprovalCode'),
        invoice_number : _.get(response, 'InvoiceNumber')
      }
    }
    this.logv2(scope, 'responseFormatterForCancelRedeem', 'Constructing response', {});
    return _.omitBy(payLoad, _.isUndefined);
  }

  getRedeemAmount(cartValue, cardBalance){
    return cartValue <= cardBalance ? cartValue : cardBalance;
  }

}

module.exports = QwikcilverHelper;