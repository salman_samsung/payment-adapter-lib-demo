"use strict";

const BaseServiceClient = require('base-lib').BaseServiceClient,
    repoInfo = require('../../../repo-info'),
    scope = `PayUAdapter#${repoInfo.version}`,
    joi = require('joi'),
    validationSchema = require('../../schema/payu-response-schema')

class PayuGatewayAdapter extends BaseServiceClient {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext)
    }
    async startTransaction(payload) {
        return {
            message: {},
            Status: { pending: true },
        }
    }
}

module.exports = PayuGatewayAdapter;