'use strict'

const BaseHelper = require('base-lib').BaseHelper,
    PayUGatewayImpl = require('./payu-gateway-impl');

class PaymentGatewayFactory {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
    }

    getPaymentGateway(gateway) {
        let me = this;
        switch (gateway.toLowerCase()) {
            case Enum.PaymentGatewayOptions.PayU:
                return new PayUGatewayImpl(me.dependencies, me.config, me.rawRequestContext);
        }
    }
}

module.exports = PaymentGatewayFactory;