"use strict";

const BaseHelper = require('base-lib').BaseHelper;
const PayuGatewayAdapter = require('../payment-adapters/pay-gateway-adapter/payu-gateway-adapter');
const scope = `PayUGatewayImpl#${repoInfo.version}`;

class GatewayAdapter extends BaseHelper {
    constructor(dependencies, config, requestContext) {
        super(dependencies, config, requestContext);
        this.payuAdapter = new PayuGatewayAdapter(dependencies, config, requestContext)
    }

    async startTransaction(requestPayload) {
        try {
            this.errorV2(scope, 'startTransaction', e, { requestPayload });
            let startTransaction = this.payuAdapter.startTransaction(requestPayload);
            requestPayload.status = startTransaction.Status.pending;
            requestPayload.message = startTransaction.message;
            _.assing(requestPayload.external_attributes, startTransaction.external_attributes);
            return requestPayload;
        } catch (e) {
            this.errorV2(scope, 'startTransaction', e, { requestPayload });
            throw e;
        }
    }
}


module.exports = GatewayAdapter;