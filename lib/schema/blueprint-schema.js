"use strict";

const joi = require('joi');

const BlueprintSchema = joi.object();

module.exports = {
  BlueprintSchema
}